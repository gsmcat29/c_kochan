/**
 * 4. Write a program that converts 27® from degrees Fahrenheit (F) to
 * degrees Celsius (C) using the following formula:
 * 
 * C = (F - 32) / 1.8
 * 
 **/

#include <stdio.h> 

int main(void)
{
	float fahr_temp = 27;
	float cels_temp = 0;

	cels_temp = (fahr_temp - 32) / 1.8;

	printf("%.2f *F ->  %.2f *C\n", fahr_temp, cels_temp);

	return 0;
}
