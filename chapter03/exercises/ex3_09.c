/*
9. Write a program to find the next largest even multiple for the following
values of i and j:

        i           j
        365         7
        12,258      23
        996         4

*/


#include <stdio.h>

int main(void)
{
    int i0 = 256, i1 = 12258, i2 = 996;
    int j0 = 7, j1 = 23, j2 = 4;
    int next_multiple;

    next_multiple = i0 + j0 - i0 % j0;
    printf("\ti\t\tj\t\tnext multiple\n");
    printf("\t%d\t\t%d\t\t%d\n", i0, j0, next_multiple);
    next_multiple = i1 + j1 - i1 % j1;
    printf("\t%d\t\t%d\t\t%d\n", i1, j1, next_multiple);
    next_multiple = i2 + j2 - i2 % j2;
    printf("\t%d\t\t%d\t\t%d\n", i2, j2, next_multiple);
  
    return 0;
}