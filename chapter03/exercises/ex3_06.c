/* 6. Write a program to evaluate the polynomial shown here:

    3x^3 - 5x^2 + 6

    for x = 2.55
*/

#include <stdio.h>

int main()
{
    float x = 2.55;
    float ans = 0;

    printf("Evaluating equation....\n");

    ans = (3 * x * x * x) - (5 * x * x) + 6;

    printf("The result is: %.2f\n", ans);

    return 0;
}