/* Program 3.1 Using the Basic Data Types */
#include <stdio.h>

int main(void)
{
	int 		integerVar = 100;
	float		floatingVar = 331.79;
	double		doubleVar = 8.44e+11;
	char		charVar = 'W';

	_Bool		boolVar = 0;

	printf("integerVar = %d\n", integerVar);
	printf("floatingVar = %f\n", floatingVar);
	printf("doubleVar = %e\n", doubleVar);
	printf("doubleVar = %g\n", doubleVar);
	printf("charVar = %c\n", charVar);

	printf("boolVar = %d\n", boolVar);

	return 0;
}