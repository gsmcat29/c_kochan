/* Program 3.5 converting between integers and floats */
// Basic conversions in c
#include <stdio.h>

int main(void)
{
	float f1 = 123.125, f2;
	int   i1, i2 = -150;
	char  c = 'a';

	i1 = f1;		// floating to integer conversion
	printf("%f assigned to an int produces %d\n", f1, i1);

	f1 = i2;		// integer to floating conversion
	printf("%d assigned to a float produces %f\n", i2, f1);

	f1 = i2 / 100;	// integer divided by integer
	printf("%d divided by 100 produces %f\n", i2, f1);

	f2 = i2 / 100.0;	// integer divided by a float
	printf("%d divided by 100.0 produces %f\n", i2, f2);

	f2 = (float) i2  / 100;	// type cast operator
	printf("(float) %d divided by 100 produces %f\n", i2, f2);

	return 0;

}