/**
 * 6. You don’t need to use an array to generate Fibonacci numbers. You can
 * simply use three variables: two to store the previous two Fibonacci
 * numbers and one to store the current one. Rewrite Program 6.3 so that
 * arrays are not used. Because you’re no longer using an array, you need to
 * display each Fibonacci number as you generate it.
 **/

// Program to generate the first 15 fibonacci numbers
#include <stdio.h>

int main(void)
{
	int fibonacci_0 = 0;	// by definition
	int fibonacci_1 = 1;	// ditto
	int fibonacci_n;

	printf("%d %d ", fibonacci_0, fibonacci_1);

	for (int i = 2; i < 15; ++i)
	{
		fibonacci_n = fibonacci_0 + fibonacci_1;
		fibonacci_0 = fibonacci_1;
		fibonacci_1 = fibonacci_n;

		printf("%d ", fibonacci_n);
	}

	printf("\n");

	return 0;	
}
