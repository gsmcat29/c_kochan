/**
 * 5. What do you expect from the following program?
 **/ 

/**
 * Before compiling:
 * 
 * Is going to perfom the summation of every value of array 10 times
 **/

#include <stdio.h>

int main(void)
{
	int numbers[10] = {1, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	int i, j;

	for (j = 0; j < 10; ++j)
		for (i = 0; i < j; ++i)
			numbers[j] += numbers[i];


	for (j = 0; j < 10; ++j)
		printf("%d ", numbers[j]);

	printf("\n");

	return 0;
}