/**
 * 3. Program 6.2 permits only 20 responses to be entered. Modify that
 * program so that any number of responses can be entered. So that the user
 * does not have to count the number of responses in the list, set up the
 * program so that the value 999 can be keyed in by the user to indicate that
 * the last response has been entered. (Hint: You can use the break
 * statement here if you want to exit your loop.)
 **/

#include <stdio.h>

int main(void)
{
    int ratingCounters[11], i, response;

    for (i = 1; i <= 10;++i)
        ratingCounters[i] = 0;

    printf("Enter your responses\n");

    /* Using a infinite loop which only condition to exit is 999 */

    while (1) {
    	scanf("%d", &response);

    	if (response == 999)
    		break;
    	else if ( response < 1 || response > 10)
    		printf("Bad response: %d\n", response);
    	else
    		++ratingCounters[response];
    }

    printf("\n\nRating\t\tNumber of Responses\n");
    printf("---------\t-------------------\n");

    for (i = 1; i <= 10; ++i)
        printf("%4d%14d\n", i, ratingCounters[i]);

    return 0;
}

