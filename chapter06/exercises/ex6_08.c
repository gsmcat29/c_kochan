/**
 * 8. Find out if your compiler supports vaiable-length arrays. If it does,
 * write a small program to test the feature out.
 **/

#include <stdio.h>

int main()
{
	int n;
	int p;

	printf("Enter a value n for the array: ");
	scanf("%d", &n);

	/* I assign the vla variable to be able to use in other statements */
	p = n;

	int numbers[n];

	int i, j;

	for (int i = 0; i < n; ++i)
	{
		numbers[i] = 0;
	}

	numbers[0] = 1;

	for (j = 0; i < p; ++j)
		for (i = 0; i < j; ++i)
			numbers[j] += numbers[i];

	for (j = 0;j < p; ++j)
		printf("%d ", numbers[j]);

	printf("\n");

	return 0;
}