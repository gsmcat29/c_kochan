/**
 * 4. Write a program that calculates the average of an array of 10 
 * floating-point values.
 **/ 

#include <stdio.h>

int main()
{
	/* initialize the numbers[] array to zero */
	float numbers[10] = {0};
	float average = 0.0;
	float sum_val = 0.0;

	/* Initialize numbers[] array with values */
	printf("Pleas enter the values for your array: \n");
	for (int i = 0; i < 10; ++i)
	{
		printf("%d >> ", i);
		scanf("%f", &numbers[i]);
		printf("\n");
		sum_val = sum_val + numbers[i];
	}

	/* Compute average of array */
	average = sum_val / 10;

	printf("The average of your array is %.2f\n", average);

	return 0;
}