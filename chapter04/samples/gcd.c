/* Program 4.7 - Program to find the greatest common divisor of two
	nonnegative integer values */

#include <stdio.h>

int main(void)
{
	int u, v, temp;

	printf("Please type in two nonnegative integers.\n");
	scanf("%d%d", &u, &v);

	while (v != 0) {
		temp = u % v;
		u = v;		
		v = temp;
	}

	/* the value u, represents the gcd of v */

	printf("Their greatest common divisor is %d\n", u);

	return 0;
}

