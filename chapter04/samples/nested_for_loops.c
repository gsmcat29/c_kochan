/* Program 4.5 - Using nested for loops */
#include <stdio.h>

int main(void)
{
	int n, number, triangularNumber, counter;

	for (counter = 1; counter <= 5; ++counter) {
		printf("What triangularNumber do you want? ");
		scanf("%d", &number);

		triangularNumber = 0;

		for (n = 1; n <= number; ++n)
			triangularNumber += n;

		printf("triangularNumber %d is %d\n\n", number, 
			triangularNumber);
	}

	return 0;
}