/* Program 4.3 - Generating a table of triangular numbers */

// Program to generate a table of triangular numbers

#include <stdio.h>

int main(void)
{
	int n, triangularNUmber;

	printf("TABLE OF TRAINGULAR NUMBERS\n\n");
	printf(" n \tSum from 1 to n\n");
	printf("---\t---------------\n");

	triangularNUmber = 0;

	for (n = 1; n <= 10; ++n) {
		triangularNUmber += n;
		printf(" %2d\t\t%2d\n", n, triangularNUmber);
	}

	return 0;
}