/* Program 4.4 - Asking the user for input */
#include <stdio.h>

int main(void)
{
	int n, number, triangularNumber;

	printf("What triangular number do you want? ");
	scanf("%d", &number);

	triangularNumber = 0;

	for (n = 1; n <= number; ++n)
		triangularNumber += n;

	printf("Triangular number %d is %d\n", number, triangularNumber);

	return 0;
}