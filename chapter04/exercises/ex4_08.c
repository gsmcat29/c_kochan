/**
 * 8. Program 4.5 allows the user to type in only five different numbers.
 * Modify that program so that the user can type in the number of triangular
 * numbers to be calculated.
 **/


#include <stdio.h>

int main(void)
{
	int n, number, triangularNumber, counter;
	int quantity;

	printf("Enter the number of triangular numbers to compute: ");
	scanf("%d", &quantity);

	for (counter = 1; counter <= quantity; ++counter) {
		printf("What triangularNumber do you want? ");
		scanf("%d", &number);

		triangularNumber = 0;

		for (n = 1; n <= number; ++n)
			triangularNumber += n;

		printf("triangularNumber %d is %d\n\n", number, 
			triangularNumber);
	}

	return 0;
}