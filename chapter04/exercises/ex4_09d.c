/**
 * 9. Rewrite Programs 4.2 through 4.5, replacing all uses of the for statement
 * with equivalent while statements. Run each program to verify that both
 * versions are identical.
 **/

#include <stdio.h>

int main(void)
{
	int n, number, triangularNumber, counter;

	counter = 1;

	while (counter <= 5) {

		printf("What triangularNumber do you want? ");
		scanf("%d", &number);

		triangularNumber = 0;

		n = 1;
		while (n <= number) {
			triangularNumber += n;
			++n;
		}

		printf("triangularNumber %d is %d\n\n", number, 
			triangularNumber);

		++counter;
	}

	return 0;
}