/*4. The factorial of an integer n, written n!, is the product of the consecutive
integers 1 through n. For example, 5 factorial is calculated as 

	5! = 5 x 4 x 3 x 2 x 1 = 120

Write a program to generate and print a table of the first 10 factorial
*/

#include <stdio.h>

int main(void)
{
	int number = 1;
	long factorial = 1;

	for (int j = 1; j <= 10; ++j)
	{
		factorial = factorial * number;
		number++;
		printf("Factorial of %d -> %ld\n", j, factorial);
	}

	return 0;

}