/**
 * 9. Rewrite Programs 4.2 through 4.5, replacing all uses of the for statement
 * with equivalent while statements. Run each program to verify that both
 * versions are identical.
 **/

#include <stdio.h>

int main(void)
{
	int n, triangularNUmber;

	printf("TABLE OF TRAINGULAR NUMBERS\n\n");
	printf(" n \tSum from 1 to n\n");
	printf("---\t---------------\n");

	triangularNUmber = 0;

	n = 1;

	while (n <= 10) {
		triangularNUmber += n;
		printf(" %2d\t\t%2d\n", n, triangularNUmber);
		++n;
	}


	return 0;
}