/**
 * 10. What would happen if you typed a negative number into Program 4.8? Try
 * it and see.
 **/

#include <stdio.h>

int main(void)
{
	int number, right_digit;

	printf("Enter your number.\n");
	scanf("%d", &number);

	while (number != 0) {
		right_digit = number % 10;
		printf("%d", right_digit);
		number = number / 10;
	}

	printf("\n");

	return 0;
}

/* ASNWER:
Due the fact that number is negative, it carries the sign to the
other digits  */
