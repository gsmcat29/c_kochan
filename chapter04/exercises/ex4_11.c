/**
 * 11. Write a program that calculates the sum of the digits of an integer. For
 * example, the sum of the digits of the number 2155 is 2 + 1 + 5 + 5 or 13.
 * The program should accept any arbitrary integer typed in by the user.
 **/

#include <stdio.h>

int main(void)
{
	int number, digit, summation = 0;

	printf("Enter number to compute: ");
	scanf("%d", &number);

	while (number > 0) {
		digit = number % 10;
		summation = summation + digit;
		number = number / 10;
	}

	printf("Summation of values -> %d\n", summation);

	return 0;
}