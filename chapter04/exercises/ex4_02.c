/* 2. Write a program to generate and display a table of n and n2, for integer
values of n ranging from 1 to 10. Be certain to print appropriate column
headings. */

#include <stdio.h>

int main(void)
{
	int number = 1, squared;
	printf("\tn\t\tn^2\n");
	printf("=====================================\n");
	for (int i = 1; i <= 10; ++i) {
		squared = number * number;
		printf("\t%3d\t\t%3d\n", number, squared);
		number++;
	}

	return 0;

}