/**
 * 4. Write a program that acts as a simple “printing” calculator. The program
 * should allow the user to type in expressions of the form
 * 
 * 		number	 operator
 * The following operators should be recognized by the program:
 * 
 * + - * / S E
 * 
 * The S operator tells the program to set the “accumulator” to the typed-in
 * number. The E operator tells the program that execution is to end. The
 * arithmetic operations are performed on the contents of the accumulator
 * with the number that was keyed in acting as the second operand. The
 * following is a “sample run” showing how the program should operate:
 * 
 * Begin Calculations
 * 10 S 				Set Accumulator to 10
 * = 10.000000			Contents of Accumulator
 * 2 /					Divide by 2
 * = 5.000000			Contents of Accumulator
 * 55 -					Subtract 55
 * -50.000000
 * 100.25 S 			Set Accumulator to 100.25
 * = 100.250000
 * 4 *					Multiply by 4
 * = 401.000000
 * 0 E 					End of program
 * = 401.000000
 * End of Calculations.
 * 
 * Make certain that the program detects division by zero and also check
 * for unknow operators
 **/ 

#include <stdio.h>

int main(void)
{
	double number;
    double accumulator = 0;
	char operator = ' ';

	printf("Begin Calculations\n");

    while (operator != 'E') {
        scanf("%lf %c", &number, &operator);
        printf("%lf %c\t\t", number, operator);

        switch (operator) {
            case 'S':
                printf("Set Accumulator to %lf\n", number);
                accumulator = number;
                printf("= %lf\t\tContents of Accumulator\n", accumulator);
                break;
            case '+':
                printf("Add %lf\n", number);
                accumulator = accumulator + number;
                printf("= %lf\n", accumulator);
                break;
            case '-':
                printf("Subtract %lf\n", number);
                accumulator = accumulator - number;
                printf("= %lf\n", accumulator);
                break;
            case '*':
                printf("Multiply by %lf\n", number);
                accumulator = accumulator * number;
                printf("= %lf\n", accumulator);
                break;
            case '/': 
                if (number == 0) {
                    printf("Divsion by zero not supported\n");
                    printf("Exit program...");
                    return 1;
                }
                printf("Divide by %lf\n", number);
                accumulator = accumulator / number;
                printf("= %lf\n", accumulator);
                break;
            case 'E': 
                printf("End of program\n");
                printf("= %lf", accumulator);
                break;
            default:
                printf("Invalid operator");
                break;
        }

    }

    printf("\nEnd of Calculations\n");

    return 0;
}