/**
 * 6. Write a program that takes an integer keyed in from the terminal and
 * extracts and displays each digit of the integer in English. So, if the user
 * types in 932, the program should display
 * 
 * 			nine three two
 * 
 * Remember to display “zero” if the user types in just a 0. (Note: This 
 * exercise is a hard one!)
 **/

#include <stdio.h>

int main()
{
	int number, digit;
	int reverse_number = 0;

	printf("Please enter a integer: ");
	scanf("%d", &number);

	/* In this case is necessary to reverse the number and then apply the
		algorithm to obtain number by number */
	while (number != 0)
	{
		reverse_number = reverse_number * 10 + number % 10;
		number = number / 10;
	}

	// Now that number is rever apply program to extrct digit by digit
	//printf("reverse_number = %d", reverse_number);

	while (reverse_number != 0)
	{
		digit = reverse_number % 10;
		reverse_number = reverse_number / 10;

		switch(digit)
		{
			case 1:
				printf("one ");		break;
			case 2:
				printf("two ");		break;
			case 3:
				printf("three ");	break;
			case 4:
				printf("four ");	break;
			case 5:
				printf("five ");	break;
			case 6:
				printf("six ");		break;
			case 7:
				printf("seven ");	break;
			case 8:
				printf("eigth ");	break;
			case 9:
				printf("nine ");	break;
			case 0:
				printf("zero ");	break;
		}
	}

	printf("\n");
	return 0;
}