/**
 * 3. Write a program that accepts two integer values typed in by the user.
 * Display the result of dividing the first integer by the second, to three-
 * decimal-place accuracy. Remember to have the program check for
 * division by zero.
 **/

#include <stdio.h>

int main(void)
{
	int value1, value2;
	float result;
	printf("Enter two integer values: ");
	scanf("%d %d", &value1, &value2);

	// type casting to avoid result with 0.000
	result = (float) value1 / value2;

	if (value2 == 0)
		printf("Division by zero not supported. Restart the program");

	printf("Result of %d / %d is: %.3f", value1, value2, result);
	printf("\n");
	
	return 0;
}