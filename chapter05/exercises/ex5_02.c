/**
 * 2. Write a program that asks the user to type in two integer values at the
 * terminal. Test these two numbers to determine if the first is evenly
 * divisible by the second and then display an appropriate message at
 * the terminal
 **/ 

#include <stdio.h>

int main(void)
{
	int value1, value2;
	printf("Enter two integer values: ");
	scanf("%d %d", &value1, &value2);

	if (value1 % value2 == 0)
		printf("%d is evenly divisible by %d\n", value1, value2);
	else
		printf("%d is not evenly divisible\n", value1);

	return 0;
}